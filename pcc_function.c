/*
 * pcc_function.c
 *
 *  Created on: May 5th, 2020
 *      Author: ryan.wostarek
 *     Version: 1.0.0
 *
 *  These functions do all the main calculations for the predictive cruise control functions.
 *  They are initiated from the 'pcc_dll.c', which acts as wrapper for this PCC code to the AVL Cruise environment
 *
 */

#include "pcc_function.h"

struct Csv_Array *csv_terrain;
int route_no;
int csv_terrain_loaded = 0;
int already_in_coast = 0;

// calculate ideal vehicle acceleration assuming EPA GEM values and truck is in neutral
double calculate_acceleration_m_s2(double vel_m_s, double incl_frac, struct Truck_Parameters *truck) {
	return -1*0.5*(1/(truck->mass_kg*(1+truck->equiv_mass_mult)))*truck->air_density_kg_m3*truck->cda_m2*pow(vel_m_s,2) -
			cos(atan(incl_frac))*9.81*truck->coef_roll_res*(1/(1+truck->equiv_mass_mult)) -
			sin(atan(incl_frac))*9.81*(1/(1+truck->equiv_mass_mult)) -
			truck->drag_force_N/(truck->mass_kg*(1+truck->equiv_mass_mult));
}

// get terrain inclination data from CSV file
struct Csv_Array *create_inclination_arrays(int route_no) {

	// find file name based on given route number and open file
	FILE *f;
	char file_name[100];
	sprintf(file_name, "profiles/%d_inclination.csv", route_no);
	f = fopen(file_name, "r");

	// check if CSV file was opened
	if (!f) {
		printf("Unable to open inclination file");
		return 0;
	}

	// create array of data structures for CSV data
	static struct Csv_Array output;

	// read file into the array
	char buf[1024];
	int row_count = -1;
	int field_count;
	while (fgets(buf, 1024, f)) {
		row_count++;
		field_count = 0;
		char *field = strtok(buf, ",");
		while (field) {
			if (field_count == 0) {
				output.data[row_count].dist_m = atof(field);
			}
			if (field_count == 1) {
				output.data[row_count].incl_frac = atof(field)/100;
			}
			field = strtok(NULL, ",");
			field_count++;
		}
	}
	fclose(f);

	// record length of CSV file
	output.length = row_count + 1;

	return &output;
}

// binary searching algorithm for finding indices for interpolation. Returns double e.g. return 4.0 -> x0 = 4 and x1 = 4, return 4.5 -> x0 = 4 and x1 = 5
double binary_search_terrain_distance(int idx0, int idx1, double dist_m, struct Csv_Point *data) {
	// check for matching end points or extrapolation needed. Use nearest value for extrapolation
	if (dist_m <= data[idx0].dist_m) {
		return (double) idx0;
	} else if (dist_m >= data[idx1].dist_m) {
		return (double) idx1;
	}

	// check if index window is only width 1, if not calculate new window
	if (idx1 - idx0 == 1) {
		return ((double) idx1 + (double) idx0) / 2;
	} else {
		int idx_middle = (idx0>>1) + (idx1>>1) + ((idx0 | idx1) & 1);
		if (dist_m == data[idx_middle].dist_m) {
			return (double) idx_middle;
		} else if (dist_m > data[idx_middle].dist_m) {
			return binary_search_terrain_distance(idx_middle, idx1, dist_m, data);
		} else {
			return binary_search_terrain_distance(idx0, idx_middle, dist_m, data);
		}
	}
}

// function that can be called after csv_terrain has been created to pull inclination for AVL simulation
double get_inclination(int route_no_input, double dist_m) {
	if (csv_terrain_loaded == 0 || route_no_input != route_no) {
			route_no = route_no_input;
			csv_terrain = create_inclination_arrays(route_no) ;
			csv_terrain_loaded = 1;
	}
	return interp_inclination_frac(dist_m, csv_terrain);
}

// interpolate inclination from CSV data. X data must be monotonically increasing with no repeated values
double interp_inclination_frac(double dist_m, struct Csv_Array *terrain) {
	int i0, i1;
	double im, x0, y0, x1, y1;
	im = binary_search_terrain_distance(0, terrain->length-1, dist_m, terrain->data);
	i0 = (int)floor(im);
	i1 = (int)ceil(im);

	x0 = terrain->data[i0].dist_m;
	y0 = terrain->data[i0].incl_frac;
	x1 = terrain->data[i1].dist_m;
	y1 = terrain->data[i1].incl_frac;

	// check if x is on interpolation grid (x values will be equal). If so, don't interpolate to avoid dividing by 0
	if (x1 == x0) {
		return y0;
	} else {
		return y0 + (y1-y0)*(dist_m - x0)/(x1-x0);
	}
}

// return sign of value, simpler version
int sign_simple(double value) {
	if (value == 0) {
		return 0;
	} else if (value > 0) {
		return 1;
	} else {
		return -1;
	}
}

// recursive function for determining if truck should coast
double look_ahead(struct Time_Step_Data *now, double time_step_s, struct Truck_Parameters *truck, struct Csv_Array *terrain) {
	// check if time/distance limit has been exceeded
	if (now->time_s > truck->look_ahead_dist_m / truck->set_speed_m_s) {
		if (now->vel_m_s > truck->set_speed_m_s - truck->speed_tol_m_s) {
			already_in_coast = 1;
			return 10;
		} else {
			already_in_coast = 0;
			return 0;
		}
	}

	// interpolate current inclination and calculate acceleration
	double incl_frac = interp_inclination_frac(now->dist_m, terrain);
	double accel_m_s2 = calculate_acceleration_m_s2(now->vel_m_s, incl_frac, truck);

	// debug statement
	/*if (now->time_s == 0) {
		printf("\n%f,%f,%f,%f,%f,%f,%f",
				now->dist_m,
				accel_m_s2,
				incl_frac,
				-1*0.5*truck->air_density_kg_m3*truck->cda_m2*pow(now->vel_m_s,2),
				-1*cos(atan(incl_frac))*9.81*truck->coef_roll_res*truck->mass_kg,
				-1*sin(atan(incl_frac))*9.81*truck->mass_kg,
				accel_m_s2*truck->mass_kg*(1+truck->equiv_mass_mult)+truck->drag_force_N);
	}*/

	// create new Time_Step_Data structure based on acceleration
	struct Time_Step_Data future;
	future.time_s = now->time_s + time_step_s;
	future.vel_m_s = now->vel_m_s + accel_m_s2 * time_step_s;
	future.dist_m = now->dist_m + (now->vel_m_s + future.vel_m_s)/2 * time_step_s;

	// check state of vehicle for exit states
	if (sign_simple(future.vel_m_s - truck->set_speed_m_s) != sign_simple(now->vel_m_s - truck->set_speed_m_s)) {
		// if truck crosses set point, check coast time vs. minimum coast time
		if (future.time_s >= truck->min_pcc_coast_time_s || already_in_coast == 1) {			// (" - set speed: %f - now vel: %f -  fut vel: %f - now diff: %f - fut diff: %f - now sgn: %d - fut sgn: %d", truck->set_speed_m_s, now->vel_m_s, future.vel_m_s, now->vel_m_s - truck->set_speed_m_s, future.vel_m_s - truck->set_speed_m_s, sign_simple(now->vel_m_s - truck->set_speed_m_s), sign_simple(future.vel_m_s - truck->set_speed_m_s));
			already_in_coast = 1;
			return 11;
		} else {
			already_in_coast = 0;
			return 1;
		}
	} else if (sign_simple(now->vel_m_s - (truck->set_speed_m_s - truck->speed_tol_m_s)) == 1 && sign_simple(future.vel_m_s - (truck->set_speed_m_s - truck->speed_tol_m_s)) != 1) {
		// if truck is within speed bounds and drops out, return 0
		already_in_coast = 0;
		return 2;
	} else if (future.vel_m_s < truck->min_pcc_speed_m_s) {
		// if truck slows to a stop, return 0
		already_in_coast = 0;
		return 3;
	} else {
		// if no exit conditions found, look ahead another step
		return look_ahead(&future, time_step_s, truck, terrain);
	}
}

// main PCC function
double pcc_main(int route_no_input, struct Time_Step_Data *init_cond, double time_step_s, struct Truck_Parameters *truck) {
	double coast_mode;

	// debug header
	/*if (init_cond->dist_m == 0) {
		printf("\n\nDistance[m],Acceleration[m/s2],Inclination[frac],AirResistance[N],RollingResistance[N],InclineResistance[N],TotalResistance[N]");
	}*/

	// check if truck is above activation speed for PCC. If not, end early and return 4
	// also check if set speed is 0, in which case end early and return 4
	if (init_cond->vel_m_s < truck->min_pcc_speed_m_s || truck->set_speed_m_s == 0) {
		already_in_coast = 0;
		return 4;
	}

	// check if truck is above set speed. If so, return 12
	if (init_cond->vel_m_s > truck->set_speed_m_s + truck->speed_tol_m_s) {
		already_in_coast = 1;
		return 12;
	}

	// get terrain data. Uses global variables to check if route number has changed and if terrain has already been loaded
	if (csv_terrain_loaded == 0 || route_no_input != route_no) {
		route_no = route_no_input;
		csv_terrain = create_inclination_arrays(route_no) ;
		csv_terrain_loaded = 1;
	}

	// look ahead and return PCC on/off
	coast_mode = look_ahead(init_cond, time_step_s, truck, csv_terrain);

	return coast_mode;
}
