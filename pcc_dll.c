/*
 * pcc_dll.c
 *
 *  Created on: Apr 9, 2020
 *      Author: ryan.wostarek
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <windows.h>

#include "black_vc.box"
#include "pcc_function.h"

int Start(int sample)
{
  return BB_IS_OF_MEMORY_TYPE;
//  return BB_IS_OF_LOGICAL_TYPE;
}

/********************** INIT-FUNCTION *******************************/
/* Is called at the beginning of each calculation task              */

int Init(int sample)
{
  return 0;
}

/******************** OPERATE-FUNCTION ******************************/
/* Is called once for each calculation (time) step                  */

int Operate(int sample)
{
	struct Time_Step_Data init_cond;
	struct Truck_Parameters truck;

	init_cond.time_s 			= 0;
	init_cond.dist_m 			= bb[sample].input[1];
	init_cond.vel_m_s 			= bb[sample].input[2];

	truck.mass_kg 				= bb[sample].input[4];
	truck.set_speed_m_s 		= bb[sample].input[5];
	truck.speed_tol_m_s 		= bb[sample].input[6];
	truck.look_ahead_dist_m		= bb[sample].input[7];
	truck.min_pcc_speed_m_s		= bb[sample].input[8];
	truck.min_pcc_coast_time_s	= bb[sample].input[9];
	truck.equiv_mass_mult		= bb[sample].input[10];
	truck.drag_force_N			= bb[sample].input[11];
	truck.air_density_kg_m3		= bb[sample].input[12];
	truck.cda_m2				= bb[sample].input[13];
	truck.coef_roll_res			= bb[sample].input[14];

	bb[sample].output[0] = pcc_main(bb[sample].input[0], &init_cond, bb[sample].input[3], &truck);
	bb[sample].output[1] = get_inclination(bb[sample].input[0], init_cond.dist_m);
	return 0;
}

/********************* RESET-FUNCTION *******************************/
/* Is called at the end of each calculation task                    */

int Reset(int sample)
{
  return 0;
}

/********************** EXIT-FUNCTION *******************************/
/* Is called at the end of the whole calculation run                */

int Exit(int sample)
{
  return 0;
}

/********************************************************************/
