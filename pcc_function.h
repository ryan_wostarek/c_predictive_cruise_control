/*
 * pcc_function.h
 *
 *  Created on: May 5th, 2020
 *      Author: ryan.wostarek
 *     Version: 1.0.0
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#define ARRAY_SIZE 200000

#ifndef PCC_FUNCTION_H_
#define PCC_FUNCTION_H_

// data for single time step of look ahead simulation
struct Time_Step_Data {
	double time_s;
	double dist_m;
	double vel_m_s;
};

// general parameters for truck and cruise control system
struct Truck_Parameters {
	double mass_kg;
	double equiv_mass_mult; // 0.04386 matches AVL/Hyliion truck model in nuetral
	double drag_force_N; // 360 N is a good starting point
	double cda_m2; // 6.0 per EPA GEM documentation
	double air_density_kg_m3; // 1.19 matches value used in AVL Cruise
	double coef_roll_res; // recommend 0.006550 to match AVL/Hyliion truck model very closely

	double set_speed_m_s; // make sure you don't set this to 0!
	double speed_tol_m_s;
	double look_ahead_dist_m;
	double min_pcc_speed_m_s;
	double min_pcc_coast_time_s;
};

// single line in the inclination file CSV
struct Csv_Point {
	double dist_m;
	double incl_frac;
};

// structure for the entire inclination CSV file
struct Csv_Array {
	struct Csv_Point data[ARRAY_SIZE];
	int length;
};

// calculate ideal vehicle acceleration assuming EPA GEM values and truck is in neutral
double calculate_acceleration_m_s2(double vel_m_s, double incl_frac, struct Truck_Parameters *truck);

// get terrain inclination data from CSV file
struct Csv_Array *create_inclination_arrays(int route_no);

// binary searching algorithm for finding indices for interpolation. Returns double e.g. return 4.0 -> x0 = 4 and x1 = 4, return 4.5 -> x0 = 4 and x1 = 5
double binary_search_terrain_distance(int idx0, int idx1, double dist_m, struct Csv_Point *data);

// function that can be called after csv_terrain has been created to pull inclination for AVL simulation
double get_inclination(int route_no_input, double dist_m);

// interpolate inclination from CSV data. X data must be monotonically increasing with no repeated values
double interp_inclination_frac(double dist_m, struct Csv_Array *terrain);

// return sign of value, simpler version
int sign_simple(double value);

// recursive function for determining if truck should coast
double look_ahead(struct Time_Step_Data *now, double time_step_s, struct Truck_Parameters *truck, struct Csv_Array *terrain);

// main PCC function
double pcc_main(int route_no_input, struct Time_Step_Data *init_cond, double time_step_s, struct Truck_Parameters *truck);

#endif /* PCC_FUNCTION_H_ */
